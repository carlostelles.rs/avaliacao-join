<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutoCategorias extends Model
{

    protected $table = 'produtoCategorias';
    
    protected $fillable = ['nome'];
    
}
