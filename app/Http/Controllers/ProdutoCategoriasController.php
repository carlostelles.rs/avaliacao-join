<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProdutoCategorias;

class ProdutoCategoriasController extends Controller
{
    
    public function index() 
    {
        return view('produtoCategoriasList', ['categorias' => ProdutoCategorias::all()]);
    }
    
    public function find($id = 0) 
    {
        $categoria = ProdutoCategorias::findOrNew($id);
        return view('produtoCategoriasForm', ['categoria' => $categoria]);
    }
    
    public function save(Request $request, $id = null) {
        try {
            $categoria = ProdutoCategorias::updateOrCreate(
                ['id' => $id],
                ['nome' => $request->input('nome')]
            );
            if ($id != $categoria->id) {
                $this->setResponseStatus(201);
                $this->setResponseMessage('Registro criado com sucesso.');
            } else {
                $this->setResponseStatus(200);
                $this->setResponseMessage('Registro atualizado com sucesso.');
            }
        } catch (Exception $ex) {
            $this->setResponseStatus($ex->getStatus());
            $this->setResponseMessage($ex->getMessage());
        }
        
        if ($this->getResponseStatus() === 201) {
            return redirect("produtoCategorias/{$categoria->id}", $this->getResponseStatus());
        } else {
            return view('produtoCategoriasForm', [
                'categoria' => $categoria,
                'return' => $this->getResponse()
            ]);
        }
    }
    
    public function delete($id) {
        if ($categoria = ProdutoCategorias::find($id)) {
            $categoria->delete();
            $this->setResponseStatus(200);
            $this->setResponseMessage('Registro excluido com sucesso.');
        } else {
            $this->setResponseStatus(404);
            $this->setResponseMessage('Registro não encontrado.');
        }
        return response()->json($this->getResponse());
    }
}