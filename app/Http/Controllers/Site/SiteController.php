<?php
namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Exception;
use App\Article;
use App\ArticleCategory;
use App\BannerItem;

class SiteController extends Controller
{
    public function home() {
        return view('inicio', [
            'banners' => $this->getBanner(1),
            'salmos' => $this->getSalmos(3)
        ]);
    }
    
    public function article($slug) {
        try {
            if (!$article = Article::all()->where('slug', $slug)->first()) {
                throw new Exception('Page not found', 404);
            }
            return view('article', ['article' => $article]);
        } catch (Exception $e) {
            return view('error', ['error' => $e]);
        }
    }
    
    public function salmos() {
        try {
            if (!$salmos = $this->getSalmos()) {
                throw new Exception('Page not found', 404);
            }
            return view('salmos', ['salmos' => $salmos]);
        } catch (Exception $e) {
            return view('error', ['error' => $e]);
        }
    }
    
    private function getBanner($bannerId) {
        return BannerItem::all()->where('banner_id', $bannerId);
    }
    
    private function getCategory($slug) {
        return ArticleCategory::all()->where('slug', $slug)->first();
    }
    
    private function getSalmos($limit = 100) {
        $category = $this->getCategory('salmos');
        
        $where = [
            ['category_id', '=', $category->id]  
        ];
        
        $order = [
            ['publication', 'desc'],
            ['id', 'desc']
        ];
        
        return Article::getArticles($where, $order, $limit);
    }
}