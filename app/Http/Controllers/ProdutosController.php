<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProdutoCategorias;
use App\Produtos;

class ProdutosController extends Controller
{
    
    public function index() 
    {
        $categorias = [];
        foreach (ProdutoCategorias::all() as $categoria) {
            $categorias[$categoria->id] = $categoria->nome;
        }
        return view('produtosList', ['produtos' => Produtos::all(), 'categorias' => $categorias]);
    }
    
    public function find($id = 0) 
    {
        $produtos = Produtos::findOrNew($id);
        return view('produtosForm', ['produto' => $produtos, 'categorias' => ProdutoCategorias::all()]);
    }
    
    public function save(Request $request, $id = null) {
        try {
            $valor = str_replace(',', '.', $request->input('valor'));
            $produto = Produtos::updateOrCreate(
                ['id' => $id],
                [
                    'nome' => $request->input('nome'),
                    'valor' => $valor,
                    'categoria_id' => $request->input('categoria_id')
                ]
            );
            if ($id != $produto->id) {
                $this->setResponseStatus(201);
                $this->setResponseMessage('Produto inserido com sucesso.');
            } else {
                $this->setResponseStatus(200);
                $this->setResponseMessage('Produto atualizado com sucesso.');
            }
        } catch (Exception $ex) {
            $this->setResponseStatus($ex->getStatus());
            $this->setResponseMessage($ex->getMessage());
        }
        
        if ($this->getResponseStatus() === 201) {
            return redirect("produtos/{$produto->id}", $this->getResponseStatus());
        } else {
            return view('produtosForm', [
                'produto' => $produto, 
                'categorias' => ProdutoCategorias::all(),
                'return' => $this->getResponse()
            ]);
        }
    }
    
    public function delete($id) {
        if ($produto = Produtos::find($id)) {
            $produto->delete();
            $this->setResponseStatus(200);
            $this->setResponseMessage('Produto excluido com sucesso.');
        } else {
            $this->setResponseStatus(404);
            $this->setResponseMessage('Registro não encontrado.');
        }
        return response()->json($this->getResponse());
    }
}