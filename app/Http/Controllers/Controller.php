<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;
    
    private $status = null;
    private $response = null;
    private $messages = [];
    
    public function setResponseStatus($param) {
        $this->status = $param;
    }
    
    public function setResponseMessage($param) {
        $this->messages[] = $param;
    }
    
    public function setResponseContent($param) {
        $this->response = $param;
    }
    
    public function getResponseStatus() {
        return $this->status;
    }

    public function getResponse($param = 'data') {
        return [
            'status' => $this->status, 
            $param => $this->response, 
            'messages' => $this->messages
        ];
    }
}
