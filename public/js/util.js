/**
 * 
 * Util Jquery para CRUD
 * @author Carlos Telles <carlostelles.noia@gmail.com>
 */

var Form = function() {
    
    var formGrid = $('.form-grid'),
        formEdit = $('.form-edit'),
        formTrash = $('.form-trash'),
        formTrashO = $('.form-trash-o'),
        formAdd = $('.form-add'),
        formBack = $('.form-back'),
        modalTrash,
        tempRegistries,
        registries;
    
    var init = function () {        
        registries = $('input[name="ID"]');
        
        actionsEnable();
        
        registries.on('click', function () {
            actionsEnable();
        });
        
        var url = formGrid.attr('action') + '/';
        
        formAdd.on('click', function () {
            window.location.href = url + 'add';
        });
        
        formEdit.on('click', function () {
            edit(url);
        });
        
        formTrash.on('click', function () {
            trash(url, 'grid');
        });
        
        formTrashO.on('click', function () {
            trash(formGrid.attr('action'), 'form');
        });
        
        formBack.on('click', function () {
            window.history.back();
        });
    };
    
    var actionsEnable = function () {
        var count = 0;
        registries.each( function () {
            if ($(this).is(':checked')) {
                count++;
            }
        });
        if (count === 0) {
            formTrash.prop('disabled', true);
            formEdit.prop('disabled', true);
        } else if (count === 1) {
            formEdit.prop('disabled', false);
            formTrash.prop('disabled', false);
        } else {
            formTrash.prop('disabled', false);
            formEdit.prop('disabled', true);
        }
    };
    
    var edit = function (url) {
        var value;
        registries.each( function () {
            if ($(this).is(':checked')) {
                value = this.value;
                return;
            }
        });
        window.location.href = url + value;
    };
    
    var trash = function (url, origin) {
        var values = [];
        
        registries.each( function () {
            if ($(this).is(':checked') && origin !== 'form') {
                var registry = {};
                registry.id = this.value;
                registry.description = $(this).attr('data-description');
                values.push(registry);
            } else if (origin === 'form') {
                var registry = {};
                registry.id = this.value;
                registry.description = $(this).attr('data-description');
                values.push(registry);
            }
        });
        
        openModalTrash(url, values, origin);
    };
    
    var openModalTrash = function (url, values, origin) {
        var itensList = '';
        
        for (var i=0; i<values.length; i++) {
            itensList += '<p><b>(' + values[i].id + ') ' + values[i].description + '</b></p>';
        }
        
        $('<div id="modal-trash" class="modal" tabindex="-1" role="dialog" aria-hidden="true">' +
            '<div class="modal-dialog">' +
                '<form method="post" action="' + url + '">' +
                    '<div class="modal-content">' +
                        '<div class="modal-header">' +
                            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                            '<h3 class="modal-title"><strong>Por favor, confirme a exclusão do(s) registro(s) abaixo:</strong></h3>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            itensList +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<button type="button" class="btn btn-effect-ripple btn-primary" onclick="Form.trash(\'' + origin + '\')">Confirmar</button>' +
                            '<button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Cancelar</button>' +
                        '</div>' +
                    '</div>' +
                '</form>' +
            '</div>' +
        '</div>').appendTo('body');

        modalTrash = $('#modal-trash');
        modalTrash.modal();
        
        closeModalTrash();
        tempRegistries = values;
    };
    
    var closeModalTrash = function () {
        modalTrash.on('hidden.bs.modal', function  () {
            modalTrash.remove(); 
            delete(tempRegistries);
            registries = $('input[name="ID"]');
            actionsEnable();
        });
    };
    
    var submitTrash = function (origin) {
        var trashForm = modalTrash.find('form'),
            url = trashForm.attr('action');
        console.log(url)
        for (var i=0; i<tempRegistries.length; i++) {
            if (origin !== 'form') {
                var urlTemp = url + tempRegistries[i].id;
            }
            var request = $.ajax({
                url: urlTemp,
                method: "DELETE",
                dataType: "application/json",
                async: false
            });    

            request.complete(function (data) {
                if (data.status === 200) {
                    var response = JSON.parse(data.responseText);
                    if (response.status == 200) {
                        $('input[name="ID"][value="' + tempRegistries[i].id + '"]').parents('tr:first').remove();
                        for (var j=0; j<response.messages.length; j++) {
                            Util.messagePop('<h4><strong>Sucesso</strong></h4> <p>' + response.messages[j] + '</p>', 'success');
                        }
                        if (origin === 'form') {
                            window.location.href = formTrashO.parents('form:first').attr('action');
                        }
                    } else {
                        for (var j=0; j<response.messages.length; j++) {
                            Util.messagePop('<h4><strong>Erro</strong></h4> <p>' + response.messages[j] + '</p>', 'danger');
                        }    
                    }
                } else {
                    for (var j=0; j<response.messages.length; j++) {
                        Util.messagePop('<h4><strong>Erro</strong></h4> <p>' + data.status + ' ' + data.statusText + '</p>', 'danger');
                    }    
                }
            });
        }
        tempRegistries = null;
        modalTrash.modal('hide');
    };
    
    return {
        init: function () {
            init();
        },
        trash: function (origin) {
            submitTrash(origin);
        }
    };
}();

$(function(){ Form.init(); });

var Util = function () {
    var messagePop = function (text, type) {
        $.bootstrapGrowl(text, {
            type: type,
            delay: 3000,
            allow_dismiss: true,
            offset: {from: 'top', amount: 20}
        });
    };
    
    return {
        messagePop: function (text, type) {
            messagePop(text, type);
        }
    };
}();
