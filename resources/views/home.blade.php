@extends('layout')

@section('rotine_title', 'HOME')

@section('content')    
<div id="page-content">
    <h1><a href="produtoCategorias">Categorias de Produtos</a></h1>
    <h1><a href="produtos">Produtos</a></h1>
</div>
@endsection