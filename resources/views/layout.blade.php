<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Avaliação Join </title>
        <meta name="description" content="Avaliação para Join Tecnologia">
        <meta name="author" content="Carlos Telles <carlostelles.noia@gmail.com>">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/plugins.css">
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/themes/custom.css">
        <script src="/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>
                    <h3 class="text-primary visible-lt-ie10"><strong>Carregando...</strong></h3>
                </div>
            </div>
            <div id="page-container" class="header-fixed-top">
                <div id="main-container">
                    <header class="navbar navbar-inverse navbar-fixed-top">
                        <ul class="nav navbar-nav-custom">
                            <li class="hidden-xs animation-fadeInQuick">
                                <a href="/"><strong>@yield('rotine_title')</strong></a>
                            </li>
                        </ul>
                    </header>
                    @yield('content')
                </div>
            </div>
        </div>

        <script src="/js/vendor/jquery-2.2.0.min.js"></script>
        <script src="/js/vendor/bootstrap.min.js"></script>
        <script src="/js/plugins.js"></script>
        <script src="/js/app.js"></script>
        <script src="/js/util.js"></script>
        @yield('javascript')
    </body>
</html>