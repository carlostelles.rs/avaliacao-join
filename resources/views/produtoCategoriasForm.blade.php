@extends('layout')

@section('rotine_title', 'CATEGORIAS DE PRODUTOS')

@section('content')    
<div id="page-content" class="">
    <div class="block">
        
        <form class="form-grid" action="/produtoCategorias/{{ (int) $categoria->id > 0 ? $categoria->id : 'add' }}" method="post">
            <input type="hidden" name="ID" value="{{ $categoria->id }}" data-description="{{ $categoria->nome }}" />
            <div class="block-title">
                <div class="block-options pull-right">
                    <button type="button" class="btn btn-effect-ripple btn-default btn-sm form-back" title="Voltar"><i class="fa fa-arrow-left"></i></button>
                    <button type="submit" class="btn btn-effect-ripple btn-primary btn-sm" title="Salvar"><i class="fa fa-check"></i></button>
                    @if ((int) $categoria->id > 0)
                    <button type="button" class="btn btn-effect-ripple btn-danger btn-sm form-trash-o" title="Excluir"><i class="fa fa-trash-o"></i></button>
                    @else
                    <button type="reset" class="btn btn-effect-ripple btn-warning btn-sm" title="Limpar"><i class="fa fa-ban"></i></button>
                    @endif
                </div>
                <h2>Formulários de Categorias de Produto</h2>
            </div>

            <div class="block-section">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="name">Nome</label>
                        <input type="text" id="nome" name="nome" class="form-control" value="{{ $categoria->nome }}" required>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </form>
        
    </div>
</div>
@endsection