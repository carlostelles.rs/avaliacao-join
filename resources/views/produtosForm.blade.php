@extends('layout')

@section('rotine_title', 'PRODUTOS')

@section('content')    
<div id="page-content" class="">
    <div class="block">
        
        <form class="form-grid" action="/produtos/{{ (int) $produto->id > 0 ? $produto->id : 'add' }}" method="post">
            <input type="hidden" name="ID" value="{{ $produto->id }}" data-description="{{ $produto->nome }}" />
            <div class="block-title">
                <div class="block-options pull-right">
                    <button type="button" class="btn btn-effect-ripple btn-default btn-sm form-back" title="Voltar"><i class="fa fa-arrow-left"></i></button>
                    <button type="submit" class="btn btn-effect-ripple btn-primary btn-sm" title="Salvar"><i class="fa fa-check"></i></button>
                    @if ((int) $produto->id > 0)
                    <button type="button" class="btn btn-effect-ripple btn-danger btn-sm form-trash-o" title="Excluir"><i class="fa fa-trash-o"></i></button>
                    @else
                    <button type="reset" class="btn btn-effect-ripple btn-warning btn-sm" title="Limpar"><i class="fa fa-ban"></i></button>
                    @endif
                </div>
                <h2>Formulários de Produto</h2>
            </div>

            <div class="block-section">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="name">Nome</label>
                        <input type="text" id="nome" name="nome" class="form-control" value="{{ $produto->nome }}" required>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="categoria_id">Categoria</label>
                        <select id="categoria_id" name="categoria_id" class="select-select2" style="width: 100%;" data-placeholder="Selecione">
                            <option></option>
                            @foreach($categorias as $categoria)
                            <option value="{{ $categoria->id }}" {{ $produto->categoria_id == $categoria->id ? 'selected' : '' }}> {{ $categoria->nome }}</option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="valor">Valor</label>
                        <input type="text" id="nome" name="valor" class="form-control" value="{{ $produto->valor }}" required>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </form>
        
    </div>
</div>
@endsection