@extends('layout')

@section('rotine_title', 'CATEGORIAS DE PRODUTOS')

@section('content')    
<div id="page-content" class="">
    <div class="block">
        
        <form class="form-grid" action="{{ Request::url() }}">
            <div class="block-title">
                <div class="block-options pull-right">
                    <button type="button" class="btn btn-effect-ripple btn-warning btn-sm form-edit" title="Editar"><i class="fa fa-pencil"></i></button>
                    <button type="button" class="btn btn-effect-ripple btn-danger btn-sm form-trash" title="Excluir"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-effect-ripple btn-info btn-sm form-add" title="Novo"><i class="fa fa-plus"></i></button>
                </div>
                <h2>Lista de Categorias de Produtos</h2>
            </div>

            <div class="block-section">
                <table class="table table-striped table-vcenter remove-margin-bottom">
                    <thead>
                        <tr>
                            <th class="text-center"></th>
                            <th>Nome</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categorias as $categoria)
                        <tr>
                            <td class="text-center">
                                <label class="csscheckbox csscheckbox-success">
                                    <input type="checkbox" name="ID" value="{{ $categoria->id }}" data-description="{{ $categoria->nome }}">
                                    <span></span>
                                </label>
                            </td>
                            <td>{{ $categoria->nome }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>    
        </form>
        
    </div>
</div>
@endsection