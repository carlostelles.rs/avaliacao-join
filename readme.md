- Para executar docker utilize seguinte comando abaixo:

sudo DB_PORT=8081 APP_PORT=8080 APP_PATH=$(pwd) docker-compose up -d

- Entrar no container docker:

sudo docker exec -it join bash

- Instalar dependencias:

composer install

- Para executar as migrations execute o camando abaixo dentro do container docker:

php artisan migrate