FROM ubuntu:16.04

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN apt-get update && \
    apt-get install -y apache2 libapache2-mod-php7.0 && \
    apt-get install -y php7.0 php7.0-intl php7.0-mysql php7.0-xml php7.0-zip && \
    apt-get install -y php-mbstring && \
    apt-get install -y git curl python-software-properties && \
    apt-get install -y fakeroot dpkg-dev && \
    apt-get install -y wget && \
    apt-get install -y npm;

RUN apt-get install nodejs -y

RUN cd /tmp && php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php && php composer-setup.php --install-dir=/bin --filename=composer

RUN cd /root && echo 'export PATH="$PATH:~/.composer/vendor/bin"' >> ./.bashrc && source ./.bashrc

RUN wget https://phar.phpunit.de/phpunit.phar && chmod +x phpunit.phar && mv phpunit.phar /usr/local/bin/phpunit

WORKDIR /var/www

RUN composer global require "laravel/installer"

EXPOSE 80

COPY 000-default.conf /etc/apache2/sites-available
COPY apache2.conf /etc/apache2/

RUN echo 'date.timezone = America/Sao_Paulo' >> /etc/php.ini

RUN sed -i 's/memory_limit.*/memory_limit = 2048M/' /etc/php.ini

RUN mkdir -p /var/log/apache2

RUN touch /var/log/apache2/error.log
RUN touch /var/log/apache2/access.log

RUN a2enmod rewrite

CMD ["apache2ctl", "-DFOREGROUND"]
