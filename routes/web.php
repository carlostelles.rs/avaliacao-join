<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::group(['prefix' => 'produtoCategorias'], function () {
    Route::get('/',             'ProdutoCategoriasController@index');
    Route::post('/add',         'ProdutoCategoriasController@save');
    Route::post('/{id}',        'ProdutoCategoriasController@save');
    Route::get('/add',          'ProdutoCategoriasController@find');
    Route::get('/{id}',         'ProdutoCategoriasController@find');
    Route::delete('/{id}',      'ProdutoCategoriasController@delete');
});
Route::group(['prefix' => 'produtos'], function () {
    Route::get('/',             'ProdutosController@index');
    Route::post('/add',         'ProdutosController@save');
    Route::post('/{id}',        'ProdutosController@save');
    Route::get('/add',          'ProdutosController@find');
    Route::get('/{id}',         'ProdutosController@find');
    Route::delete('/{id}',      'ProdutosController@delete');
});